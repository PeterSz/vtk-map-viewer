﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Drawing;
using System.Threading;

using Kitware.VTK;

using VtkMap.Renderer;

using VTKMapLib;

namespace VtkMap
{
    public class Map
    {
        private IRenderer renderer;
        private Dictionary<string, vtkProp> actors = new Dictionary<string, vtkProp>();
        private double legendWidth = 50;
        private Thread rendering;

        public uint RefreshTimeout {
            get
            {
                return renderer.RefreshTimeout;
            }
            set
            {
                renderer.RefreshTimeout = value;
            }
        }

        public Map(byte[][] map, Dictionary<int, Color> colorMap = null, bool transparency = false)
        {
            var renderer = new Renderer2D();
            renderer.SetData(map);
            renderer.ColorMap = colorMap;
            renderer.TransparencyEnabled = transparency;
            this.renderer = renderer;
        }

        public Map(byte[][][] map, Dictionary<int, Color> colorMap = null, bool transparency = false)
        {
            var renderer = new Renderer3D();
            renderer.SetData(map);
            renderer.ColorMap = colorMap;
            renderer.TransparencyEnabled = transparency;
            this.renderer = renderer;
        }

        public void SetWindowHandle(IntPtr hWindow)
        {
            renderer.WindowHandle=hWindow;
        }

        public void SetRenderingRect(Rectangle rect)
        {
            renderer.Rect = rect;
        }

        public void Draw(IntPtr? windowHandle = null)
        {
            if(windowHandle.HasValue) SetWindowHandle(windowHandle.Value);
            rendering = new Thread(new ThreadStart(this.renderer.Render));
            rendering.Start();
        }

        public void UpdateData(byte[][][] map)
        {
            ((Renderer3D)renderer).UpdateData(map);
        }

        public void UpdateData(byte[][] map)
        {
            ((Renderer2D)renderer).UpdateData(map);
        }

        public void RenderLine(string name, List<MapPoint> points, float width = 6, IEnumerable<double> costs = null)
        {
            renderer.NewLine(name, points, width, costs);
        }

        public void RenderPawn(string name, Shapes shape, MapPoint point, Color color)
        {
            renderer.NewPawn(name, shape, point, color);
        }

        public void MovePawns (Dictionary<string, MapPoint> changes)
        {
            renderer.MoveActors(changes);
        }
    }
}
