﻿using System;
using System.Drawing;

using Kitware.VTK;

namespace VtkMap.Renderer
{
    class Renderer2D : Renderer<byte[][]>
    {
        private vtkPlaneSource plane;

        public Renderer2D()
            : base()
        {
            var interaction = vtkInteractorStyleUnicam.New();
            interactor.SetInteractorStyle(interaction);
            interaction.Dispose();
        }

        public override void Render()
        {
            int x = this.data.GetLength(0),
                y = this.data[0].GetLength(0);

            // plane
            this.Actors.Add("Map", this.CreatePlane(x, y));

            this.Redraw();
        }

        private vtkActor CreatePlane(int x, int y)
        {
            plane = vtkPlaneSource.New();
            plane.SetXResolution(x);
            plane.SetYResolution(y);
            plane.SetOrigin(0, 0, 0);
            plane.SetPoint1(x, 0, 0);
            plane.SetPoint2(0, y, 0);

            //colors
            var cellData = vtkFloatArray.New();
            var colors = vtkLookupTable.New();
            int size;

            if (this.ColorMap == null)
            {
                size = x * y + 1;
                colors.SetNumberOfTableValues(size);
                colors.Build();
                for (int i = 1; i <= size; i++)
                {
                    cellData.InsertNextValue(i);
                }

                colors.SetTableValue(0, 0.0, 0.0, 0.0, 1);
                for (int i = 0; i < x; i++)
                {
                    for (int j = 0; j < y; j++)
                    {
                        var mapVal = this.data[i][j];
                        colors.SetTableValue(i * x + j + 1, mapVal, mapVal, mapVal, 1);
                    }
                }
            }
            else
            {
                size = this.ColorMap.Count;
                colors.SetNumberOfTableValues(size);
                colors.Build();

                for (int i = 0; i < x; i++)
                {
                    for (int j = 0; j < y; j++)
                    {
                        if (this.ColorMap != null)
                            cellData.InsertNextValue((float)this.data[i][j]);
                        else
                            cellData.InsertNextValue((float)this.data[i][j] / 255.0f);
                    }
                }

                foreach (var color in this.ColorMap)
                {
                    colors.SetTableValue(color.Key, (double)color.Value.R / 255.0, (double)color.Value.G / 255.0, (double)color.Value.B / 255.0, (double)color.Value.A / 255.0);
                }
            }

            plane.Update();
            plane.GetOutput().GetCellData().SetScalars(cellData);

            // mapper
            var planeMapper = vtkPolyDataMapper.New();
            planeMapper.SetInputConnection(plane.GetOutputPort());

            planeMapper.SetScalarRange(0, size - 1);
            planeMapper.SetLookupTable(colors);

            //labels


            // actor
            var planeActor = vtkActor.New();
            planeActor.SetMapper(planeMapper);
            return planeActor;
        }

        protected override void Update()
        {
            int x = this.data.GetLength(0),
                y = this.data[0].GetLength(0);

            var cellData = vtkFloatArray.New();
            int size;

            if (this.ColorMap == null)
            {
                size = x * y + 1;
                for (int i = 1; i <= size; i++)
                {
                    cellData.InsertNextValue(i);
                }
            }
            else
            {
                size = this.ColorMap.Count;

                for (int i = 0; i < x; i++)
                {
                    for (int j = 0; j < y; j++)
                    {
                        if (this.ColorMap != null)
                            cellData.InsertNextValue((float)this.data[i][j]);
                        else
                            cellData.InsertNextValue((float)this.data[i][j] / 255.0f);
                    }
                }
            }

            plane.Update();
            plane.GetOutput().GetCellData().SetScalars(cellData);

            cellData.Dispose();
            base.Update();
        }
    }
}
