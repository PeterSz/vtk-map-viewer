﻿namespace VtkMap.Renderer
{
    public enum Shapes
    {
        Cube,

        Sphere,

        Cone,

        Cylinder
    }
}