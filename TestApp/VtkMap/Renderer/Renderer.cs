﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Kitware.VTK;

using VTKMapLib;

namespace VtkMap.Renderer
{
    public abstract class Renderer<T> : IRenderer
    {
        protected T data;
        protected vtkRenderWindow renderWindow;
        protected vtkRenderer renderer;
        protected vtkRenderWindowInteractor interactor;
        protected bool modified = false;
        protected int timerId = -1;
        protected uint timeout = 0;
        protected vtkTextActor fpsTextActor;

        public IDictionary<string, vtkActor> Actors { get; protected set; }
        public IDictionary<string, vtkProp> Props { get; protected set; }
        public bool TransparencyEnabled { get; set; }
        public Dictionary<int, Color> ColorMap { get; set; }
        public Rectangle Rect { get; set; }
        public IntPtr WindowHandle { get; set; }
        
        // Refresh ratio for data updates in miliseconds (0 - updates disabled)
        public uint RefreshTimeout
        {
            get
            {
                return timeout;
            }
            set
            {
                if (timerId >= 0)
                {
                    interactor.DestroyTimer();
                }
                timerId = interactor.CreateRepeatingTimer(value);
                timeout = value;
            }
        }

        protected Renderer()
        {
            this.renderer = vtkRenderer.New();
            this.renderWindow = vtkRenderWindow.New();
            this.interactor = vtkRenderWindowInteractor.New();
            this.renderWindow.AddRenderer(this.renderer);
            this.interactor.SetRenderWindow(this.renderWindow);
            this.Actors = new Dictionary<string, vtkActor>();
            this.Props = new Dictionary<string, vtkProp>();
            this.Rect = new Rectangle(0, 0, 1150, 600);
        }
        public virtual void SetData(T data)
        {
            this.data = data;
        }
        public virtual void UpdateData(T data)
        {
            this.data = data;
            modified = true;
        }

        public abstract void Render();

        protected void Redraw()
        {
            if(WindowHandle != null) this.renderWindow.SetParentId(WindowHandle);
            this.renderWindow.SetSize(Rect.Width, Rect.Height);
            this.renderWindow.SetPosition(Rect.X, Rect.Y);
            if (this.TransparencyEnabled) this.SetupEnvironmentForDepthPeeling(renderWindow, renderer, 100, 0.1);
            this.renderer.SetBackground(0.2, 0.3, 0.4);

            foreach (var actor in this.Actors)
            {
                this.renderer.AddActor(actor.Value);
            }
            foreach (var actor in this.Props)
            {
                this.renderer.AddActor(actor.Value);
            }

            this.renderer.ResetCamera();
            this.renderWindow.Render();

            this.interactor.Initialize();

            if(RefreshTimeout > 0) timerId = interactor.CreateRepeatingTimer(RefreshTimeout);
            interactor.TimerEvt += this.UpdateEvent;

            this.interactor.Start();
        }

        protected void CreateLegend(string name, vtkMapper polyMapper)
        {
            var legend = vtkScalarBarActor.New();
            legend.SetLookupTable(polyMapper.GetLookupTable());
            legend.SetTitle(name);
            legend.SetNumberOfLabels(8);
            legend.SetMaximumWidthInPixels(50);
            legend.SetPosition(0, 0.02);

            this.Props.Add(name + " legend", legend);
        }

        public void Remove(string name)
        {
            if (this.Actors.Keys.Contains(name)) this.Actors.Remove(name);
            if (this.Actors.Keys.Contains(name + "legend")) this.Actors.Remove(name + "legend");
        }

        /**
        * Setup the rendering environment for depth peeling (general depth peeling
        * support is requested).
        * @see IsDepthPeelingSupported()
        * @param renderWindow a valid openGL-supporting render window
        * @param renderer a valid renderer instance
        * @param maxNoOfPeels maximum number of depth peels (multi-pass rendering)
        * @param occulusionRation the occlusion ration (0.0 means a perfect image,
        * >0.0 means a non-perfect image which in general results in faster rendering)
        * @return TRUE if depth peeling could be set up
        */
        bool SetupEnvironmentForDepthPeeling(vtkRenderWindow renderWindow, vtkRenderer renderer, int maxNoOfPeels, double occlusionRatio)
        {
            if (renderWindow == null || renderer == null)
                return false;

            // 1. Use a render window with alpha bits (as initial value is 0 (false)):
            renderWindow.SetAlphaBitPlanes(1);

            // 2. Force to not pick a framebuffer with a multisample buffer
            // (as initial value is 8):
            renderWindow.SetMultiSamples(0);

            // 3. Choose to use depth peeling (if supported) (initial value is 0 (false)):
            renderer.SetUseDepthPeeling(1);

            // 4. Set depth peeling parameters
            // - Set the maximum number of rendering passes (initial value is 4):
            renderer.SetMaximumNumberOfPeels(maxNoOfPeels);
            // - Set the occlusion ratio (initial value is 0.0, exact image):
            renderer.SetOcclusionRatio(occlusionRatio);

            return true;
        }

        protected void AddFpsCounter()
        {
            fpsTextActor = vtkTextActor.New();
            renderer.UpdateEvt += this.FpsEvent;
            renderer.AddActor(fpsTextActor);
        }

        protected void FpsEvent(vtkObject sender, vtkObjectEventArgs e)
        {
            vtkRenderer ren = (vtkRenderer)sender;
            double fps = 1.0 / ren.GetLastRenderTimeInSeconds();
            //sprintf(this->TextBuff,"%.1f", fps);
            string TextBuff = fps.ToString("%.1f");
            this.fpsTextActor.SetInput(TextBuff);
        }

        protected virtual void Update()
        {
            interactor.Render();
        }

        protected void UpdateEvent(vtkObject sender, vtkObjectEventArgs e)
        {
            if (modified)
            {
                this.Update();
                modified = false;
            }
        }

        protected void RenderLabels(vtkPoints points)
        {
            var labelPolyData = vtkPolyData.New();
            labelPolyData.SetPoints(points);
            var labelMapper = vtkLabeledDataMapper.New();
            labelMapper.SetInput(labelPolyData); // Note: If you're using VTK from master (6.x), this is SetInputData(...)
            var labelActor = vtkActor2D.New();
            labelActor.SetMapper(labelMapper);
            this.renderer.AddActor2D(labelActor);
        }

        public void NewLine(string name, IEnumerable<MapPoint> pointsList, float width = 6, IEnumerable<double> costs = null)
        {
            var polyMapper = vtkPolyDataMapper.New();

            var points = vtkPoints.New();
            foreach (var point in pointsList)
            {
                points.InsertNextPoint(point.X, point.Y, point.Z);
            }

            var line = vtkPolyLine.New();
            line.GetPointIds().SetNumberOfIds(points.GetNumberOfPoints());
            for (int i = 0; i < points.GetNumberOfPoints(); i++)
            {
                line.GetPointIds().SetId(i, i);
            }

            var cells = vtkCellArray.New();
            cells.InsertNextCell(line);

            var data = vtkPolyData.New();
            data.SetPoints(points);
            data.SetLines(cells);

            if (costs != null)
            {
                var costScalars = vtkDoubleArray.New();
                costScalars.SetName(name);

                foreach (var cost in costs)
                {
                    costScalars.InsertNextValue(cost);
                }

                data.GetPointData().SetScalars(costScalars);

                this.CreateLegend(name, polyMapper);
            }

            polyMapper.SetInput(data);

            //line actor
            var lineActor = vtkActor.New();
            lineActor.SetMapper(polyMapper);
            lineActor.GetProperty().SetLineWidth(width);

            this.Actors.Add(name, lineActor);
        }

        public void NewPawn(string name, Shapes shape, MapPoint point, Color color)
        {
            vtkPolyDataAlgorithm source;
            switch (shape)
            {
                case Shapes.Cube:
                    var cube = vtkCubeSource.New();
                    source = cube;
                    cube.SetCenter(point.X + 0.5, point.Y + 0.5, point.Z + 0.5);
                    cube.SetBounds(0, 0.5, 0, 0.5, 0, 0.5);
                    break;
                case Shapes.Sphere:
                    var sphere = vtkSphereSource.New();
                    sphere.SetCenter(point.X + 0.5, point.Y + 0.5, point.Z + 0.5);
                    sphere.SetRadius(0.5);
                    source = sphere;
                    break;
                case Shapes.Cone:
                    var cone = vtkConeSource.New();
                    cone.SetCenter(point.X + 0.5, point.Y + 0.5, point.Z + 0.5);
                    cone.SetHeight(0.5);
                    cone.SetRadius(0.5);
                    source = cone;
                    break;
                case Shapes.Cylinder:
                    var cylinder = vtkCylinderSource.New();
                    cylinder.SetCenter(point.X + 0.5, point.Y + 0.5, point.Z + 0.5);
                    cylinder.SetHeight(0.5);
                    cylinder.SetRadius(0.5);
                    source = cylinder;
                    break;
                default:
                    var cube2 = vtkCubeSource.New();
                    cube2.SetCenter(point.X + 0.5, point.Y + 0.5, point.Z + 0.5);
                    cube2.SetCenter(point.X + 0.5, point.Y + 0.5, point.Z + 0.5);
                    cube2.SetBounds(0, 0.5, 0, 0.5, 0, 0.5);
                    source = cube2;
                    break;
            }

            vtkPolyDataMapper mapper = vtkPolyDataMapper.New();
            mapper.SetInputConnection(source.GetOutputPort());
 
            vtkActor actor = vtkActor.New();
            actor.SetMapper(mapper);
            actor.GetProperty().SetColor(color.R / 255.0, color.G / 255.0, color.B / 255.0);
            actor.GetProperty().SetOpacity(color.A/255.0);

            source.Dispose();
            mapper.Dispose();

            this.Actors.Add(name, actor);
        }

        public void MoveActors(Dictionary<string, MapPoint> changes)
        {
            foreach (var change in changes)
            {
                var transform = vtkTransform.New();
                transform.PostMultiply();
                transform.Translate(change.Value.X, change.Value.Y, change.Value.Z);
                Actors[change.Key].SetUserTransform(transform);
            }
        }
    }
}
