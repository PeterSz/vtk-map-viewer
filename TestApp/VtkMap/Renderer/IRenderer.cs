﻿using System;
using System.Collections.Generic;
using System.Drawing;

using Kitware.VTK;

using VTKMapLib;

namespace VtkMap.Renderer
{
    interface IRenderer
    {
        IDictionary<string, vtkActor> Actors { get; }
        uint RefreshTimeout { get; set; }

        Rectangle Rect { get; set; }
        IntPtr WindowHandle { get; set; }

        void Render();
        void NewLine(string name, IEnumerable<MapPoint> pointsList, float width = 6, IEnumerable<double> costs = null);
        void NewPawn(string name, Shapes shape, MapPoint point, Color color);
        void MoveActors(Dictionary<string, MapPoint> changes);
    }
}
