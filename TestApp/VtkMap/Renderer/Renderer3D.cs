﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Kitware.VTK;

using VTKMapLib;

namespace VtkMap.Renderer
{
    class Renderer3D : Renderer<byte[][][]>
    {
        private float size3DCubes = 0.7f;
        private float spacer3DCubes = 1.0f;
        private vtkPolyData polyData;
        private vtkDataSetMapper mapper;

        public Renderer3D()
            : base()
        {
            var interaction = vtkInteractorStyleTerrain.New();
            interactor.SetInteractorStyle(interaction);
            interaction.Dispose();
        }

        public override void Render()
        {
            // Random rand = new Random(DateTime.Now.Second);
            int x = data.GetLength(0), y = data[0].GetLength(0), z = data[0][0].GetLength(0);
            var cellData = vtkFloatArray.New();

            vtkUnsignedCharArray cubeColors = vtkUnsignedCharArray.New();

            if (ColorMap == null)
            {
                cubeColors.SetNumberOfComponents(4);
                cubeColors.SetName("color");
                cellData.Dispose();
            }
            else
            {
                cubeColors.Dispose();
            }

            int numberOfCells = 0;
            var appendFilter = vtkAppendPolyData.New();

            for (ushort k = 0; k < z; k++)
            {
                for (ushort j = 0; j < y; j++)
                {
                    for (uint i = 0; i < x; i++)
                    {
                        //if (ColorMap != null && ColorMap[data[i][j][k]].A > 0)
                        //{
                            numberOfCells++;

                            var newBox = vtkCubeSource.New();
                            newBox.SetCenter(i * spacer3DCubes + spacer3DCubes / 2.0, j * spacer3DCubes + spacer3DCubes / 2.0, k * spacer3DCubes + spacer3DCubes / 2.0);
                            newBox.SetXLength(size3DCubes);
                            newBox.SetYLength(size3DCubes);
                            newBox.SetZLength(size3DCubes);
                            newBox.Update();
                            
                            appendFilter.AddInputConnection(newBox.GetOutputPort());
                            newBox.Dispose();

                            if (ColorMap != null)
                            {
                                cellData.InsertNextValue(this.data[i][j][k]);
                                cellData.InsertNextValue(this.data[i][j][k]);
                                cellData.InsertNextValue(this.data[i][j][k]);
                                cellData.InsertNextValue(this.data[i][j][k]);
                                cellData.InsertNextValue(this.data[i][j][k]);
                                cellData.InsertNextValue(this.data[i][j][k]);
                            }
                            else
                            { 
                                cubeColors.InsertNextTuple4(this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, 1.0);
                                cubeColors.InsertNextTuple4(this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, 1.0);
                                cubeColors.InsertNextTuple4(this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, 1.0);
                                cubeColors.InsertNextTuple4(this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, 1.0);
                                cubeColors.InsertNextTuple4(this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, 1.0);
                                cubeColors.InsertNextTuple4(this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, 1.0);
                            }
                        //}
                    }
                }
            }

            polyData = appendFilter.GetOutput();
            
            if (ColorMap != null)
            {
                polyData.Update();
                polyData.GetCellData().SetScalars(cellData);
                cellData.Dispose();
            }
            else
            {
                polyData.GetCellData().AddArray(cubeColors);
                polyData.GetCellData().SetActiveScalars("color");
                cubeColors.Dispose();
            }

            mapper = vtkDataSetMapper.New();
            mapper.SetInput(polyData);
            if (ColorMap != null)
            {
                var colors = vtkLookupTable.New();
                colors.SetNumberOfTableValues(ColorMap.Count);
                colors.Build();
                foreach (var color in this.ColorMap)
                {
                    colors.SetTableValue(color.Key, (double)color.Value.R / 255.0, (double)color.Value.G / 255.0, (double)color.Value.B / 255.0, (double)color.Value.A / 255.0);
                }
                mapper.SetLookupTable(colors);
                mapper.SetScalarRange(0, ColorMap.Count - 1);
            }

            // actor
            var actor = vtkActor.New();
            actor.SetMapper(mapper);
            //actor.GetProperty().SetOpacity(0.9f);
            this.Actors.Add("Map", actor);

            //fps
            //AddFpsCounter(renderer);

            //axes
            var axes = vtkCubeAxesActor.New();

            axes.SetBounds(0, x, 0, y, 0, z);
            axes.DrawXGridlinesOn();
            axes.DrawYGridlinesOn();
            axes.DrawZGridlinesOn(); 
            axes.SetCamera(renderer.GetActiveCamera());
            renderer.AddActor(axes);

            this.Redraw();
        }

        public void RenderHexahedron()
        {
            // Random rand = new Random(DateTime.Now.Second);
            int x = data.GetLength(0), y = data[0].GetLength(0), z = data[0][0].GetLength(0);
            var points = vtkPoints.New();
            var cellData = vtkFloatArray.New();

            vtkUnsignedCharArray cubeColors = vtkUnsignedCharArray.New();

            if (ColorMap != null)
            {
                cubeColors.SetNumberOfComponents(4);
                cubeColors.SetName("color");
            }

            int numberOfCells = 0;

            //List<MapPoint> list = new List<MapPoint>();
            // Step through all map cells
            for (ushort k = 0; k < z; k++)
            {
                for (ushort j = 0; j < y; j++)
                {
                    for (uint i = 0; i < x; i++)
                    {
                        if (ColorMap != null && ColorMap[data[i][j][k]].A > 0)
                        {
                            numberOfCells++;
                            this.CteatePoints(points, i, j, k);

                            if (ColorMap != null)
                            {
                                cellData.InsertNextValue(this.data[i][j][k]);
                            }
                            else
                                cubeColors.InsertNextTuple4(this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, 1.0);
                        }
                    }
                }
            }

            //create the hexahedron
            var theHex = new List<vtkVoxel>();

            for (int i = 0; i < x * y * z; i++)
            {
                theHex.Add(vtkVoxel.New());
                theHex.Last().GetPointIds().SetNumberOfIds(8);
                for (int j = 0; j < 8; j++)
                {
                    theHex.Last().GetPointIds().SetId(j, j + (i * 8));  //this mapsinternal id 0-7 to global point Id
                }
            }

            //create an unstructured grid for the hexahedron
            vtkUnstructuredGrid theGrid = vtkUnstructuredGrid.New();
            theGrid.Allocate(numberOfCells, 1000);
            for (int i = 0; i < numberOfCells; i++)
            {
                theGrid.InsertNextCell(theHex[i].GetCellType(), theHex[i].GetPointIds());
            }
            theGrid.SetPoints(points);

            if (ColorMap != null)
            {
                theGrid.Update();
                theGrid.GetCellData().SetScalars(cellData);
            }
            else
            {
                theGrid.GetCellData().AddArray(cubeColors);
                theGrid.GetCellData().SetActiveScalars("color");
            }

            //create a mapper for the hexahedron
            var theHexMapper = vtkDataSetMapper.New();
            theHexMapper.SetInput(theGrid);

            if (ColorMap != null)
            {
                var colors = vtkLookupTable.New();
                colors.SetNumberOfTableValues(ColorMap.Count);
                colors.Build();
                foreach (var color in this.ColorMap)
                {
                    colors.SetTableValue(color.Key, (double)color.Value.R / 255.0, (double)color.Value.G / 255.0, (double)color.Value.B / 255.0, (double)color.Value.A / 255.0);
                }
                theHexMapper.SetLookupTable(colors);
                theHexMapper.SetScalarRange(0, ColorMap.Count - 1);
            }

            // actor
            var actor = vtkActor.New();
            actor.SetMapper(theHexMapper);
            //actor.GetProperty().SetOpacity(0.9f);
            this.Actors.Add("Map", actor);

            //fps
            AddFpsCounter();

            //axes
            var axes = vtkCubeAxesActor.New();

            axes.SetBounds(0, x, 0, y, 0, z);
            axes.DrawXGridlinesOn();
            axes.DrawYGridlinesOn();
            axes.DrawZGridlinesOn();
            axes.SetCamera(renderer.GetActiveCamera());
            renderer.AddActor(axes);

            this.RenderLabels(points);

            this.Redraw();
        }

        private void CteatePoints(vtkPoints points, uint i, ushort j, ushort k)
        {
            // Setup the coordinates of eight points
            // (the two faces must be in counter clockwise order as viewd from the outside)

            points.InsertNextPoint(
                i * this.spacer3DCubes - this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f,
                j * this.spacer3DCubes - this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f,
                (k * this.spacer3DCubes - this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f));
            points.InsertNextPoint(
                i * this.spacer3DCubes + this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f,
                j * this.spacer3DCubes - this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f,
                (k * this.spacer3DCubes - this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f));
            points.InsertNextPoint(
                i * this.spacer3DCubes - this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f,
                j * this.spacer3DCubes + this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f,
                (k * this.spacer3DCubes - this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f));
            points.InsertNextPoint(
                i * this.spacer3DCubes + this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f,
                j * this.spacer3DCubes + this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f,
                (k * this.spacer3DCubes - this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f));
            points.InsertNextPoint(
                i * this.spacer3DCubes - this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f,
                j * this.spacer3DCubes - this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f,
                (k * this.spacer3DCubes + this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f));
            points.InsertNextPoint(
                i * this.spacer3DCubes + this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f,
                j * this.spacer3DCubes - this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f,
                (k * this.spacer3DCubes + this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f));
            points.InsertNextPoint(
                i * this.spacer3DCubes - this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f,
                j * this.spacer3DCubes + this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f,
                (k * this.spacer3DCubes + this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f));
            points.InsertNextPoint(
                i * this.spacer3DCubes + this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f,
                j * this.spacer3DCubes + this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f,
                (k * this.spacer3DCubes + this.size3DCubes / 2.0f + this.spacer3DCubes / 2.0f));
        }

        public override void UpdateData(byte[][][] data)
        {
            base.UpdateData(data);
        }

        protected override void Update()
        {
            int x = data.GetLength(0), y = data[0].GetLength(0), z = data[0][0].GetLength(0);
            var cellData = vtkFloatArray.New();

            vtkUnsignedCharArray cubeColors = vtkUnsignedCharArray.New();

            if (ColorMap == null)
            {
                cubeColors.SetNumberOfComponents(4);
                cubeColors.SetName("color");
                cellData.Dispose();
            }
            else
            {
                cubeColors.Dispose();
            }

            for (ushort k = 0; k < z; k++)
            {
                for (ushort j = 0; j < y; j++)
                {
                    for (uint i = 0; i < x; i++)
                    {
                        //if (ColorMap != null && ColorMap[data[i][j][k]].A > 0)
                        //{

                        if (ColorMap != null)
                        {
                            cellData.InsertNextValue(this.data[i][j][k]);
                            cellData.InsertNextValue(this.data[i][j][k]);
                            cellData.InsertNextValue(this.data[i][j][k]);
                            cellData.InsertNextValue(this.data[i][j][k]);
                            cellData.InsertNextValue(this.data[i][j][k]);
                            cellData.InsertNextValue(this.data[i][j][k]);
                        }
                        else
                        {
                            cubeColors.InsertNextTuple4(this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, 1.0);
                            cubeColors.InsertNextTuple4(this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, 1.0);
                            cubeColors.InsertNextTuple4(this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, 1.0);
                            cubeColors.InsertNextTuple4(this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, 1.0);
                            cubeColors.InsertNextTuple4(this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, 1.0);
                            cubeColors.InsertNextTuple4(this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, this.data[i][j][k] / 255.0, 1.0);
                        }
                        //}
                    }
                }
            }

            if (ColorMap != null)
            {
                polyData.Update();
                polyData.GetCellData().SetScalars(cellData);
            }
            else
            {
                polyData.GetCellData().AddArray(cubeColors);
                polyData.GetCellData().SetActiveScalars("color");
            }

            base.Update();
        }
    }
}
