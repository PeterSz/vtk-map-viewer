﻿using Kitware.VTK;

namespace VtkMap
{
    class vtkFPSCallback : Kitware.VTK.vtkCommand
    {
        public vtkTextActor TextActor { get; set; }

        //public vtkFPSCallback() : base()
        //{ }

        //public static vtkFPSCallback New()
        //{ return new vtkFPSCallback(); }


        public override void Execute(
            [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.CustomMarshaler, MarshalTypeRef = typeof(vtkObjectMarshaler))]
            vtkObject caller, uint eventId, System.IntPtr data)
        {
            vtkRenderer ren = (vtkRenderer)caller;
            double fps = 1.0 / ren.GetLastRenderTimeInSeconds();
            //sprintf(this->TextBuff,"%.1f", fps);
            string TextBuff = fps.ToString("%.1f");
            this.TextActor.SetInput(TextBuff);
        }
    }
}


// public class TestObserver : Kitware.VTK.vtkCommand
//  {
//     /// <summary>
//     /// Override vtkCommand::Execute
//     /// </summary>
//public override void Execute(
//      [System.Runtime.InteropServices.MarshalAs(System.Runtime.InteropServices.UnmanagedType.CustomMarshaler,
//        MarshalTypeRef = typeof(Kitware.VTK.vtkObjectMarshaler))]
//      Kitware.VTK.vtkObject caller, uint eventId, System.IntPtr data)
//    {
//      System.Console.WriteLine("");
//      System.Console.WriteLine("TestObserver.Execute called...");
//      System.Console.WriteLine(System.String.Format("  caller='{0}'", caller));
//      System.Console.WriteLine(System.String.Format("  eventId='{0}'", eventId));
//      System.Console.WriteLine(System.String.Format("  data='{0}'", data));

//      Kitware.VTK.vtkObject ob = caller;
//      if (ob.GetDebug() != 0)
//      {
//        System.Console.WriteLine("  caller Debug is ON");
//      }
//      else
//      {
//        System.Console.WriteLine("  caller Debug is OFF");
//      }
//      System.Console.WriteLine(System.String.Format("  caller MTime is {0}\n", ob.GetMTime()));

//      System.Console.WriteLine("");
//    }
//  }