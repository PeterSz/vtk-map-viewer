﻿#define _3D

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Timers;
using System.Threading.Tasks;
using VtkMap;
using VtkMap.Renderer;

namespace VTKMapLib
{
    class Program
    {
        static private Random rnd = new Random(DateTime.Now.Second);
        static private Map map;
        private static Timer timer;

        static void Main(string[] args)
        {
            Console.SetWindowSize(70, 50);
            int x = 30, y = 30, z = 30;

        #if _3D
            byte[][][] mapMatrixl = new byte[x][][];
            for (int i = 0; i < x; i++)
            {
                mapMatrixl[i] = new byte[y][];
                for (int j = 0; j < y; j++)
                {
                    mapMatrixl[i][j] = new byte[z];
                    for (int k = 0; k < z; k++)
                    {
                        mapMatrixl[i][j][k] = (byte)(i % 5);
                    }
                }
            }
        #else
            byte[][] mapMatrixl = new byte[x][];
            for (int i = 0; i < x; i++)
            {
                mapMatrixl[i] = new byte[y];
                for (int j = 0; j < y; j++)
                {
                    mapMatrixl[i][j] = (byte)(i % 5);
                }
            }
         #endif

            Dictionary<int, Color> colorMap = new Dictionary<int, Color>();
            colorMap.Add(0, Color.FromArgb(255, Color.Red));
            colorMap.Add(1, Color.FromArgb(255, Color.Green));
            colorMap.Add(2, Color.FromArgb(0, Color.Blue));
            colorMap.Add(3, Color.FromArgb(255, Color.Yellow));
            colorMap.Add(4, Color.FromArgb(0, Color.DeepPink));

            var points = new List<MapPoint>();
            points.Add(new MapPoint { X = 1.0, Y = 1.0, Z = 0.0 });
            points.Add(new MapPoint { X = 2.0, Y = 1.0, Z = 0.0 });
            points.Add(new MapPoint { X = 2.0, Y = 3.0, Z = 0.0 });

            map = new Map(mapMatrixl, colorMap, false);
            map.RefreshTimeout = 1000;

            map.RenderLine("Line", points);
            map.RenderPawn("Pawn", Shapes.Cone, new MapPoint(-1, -1, -1), Color.DeepSkyBlue);

            timer = new Timer(1000);

            //set the window handle if you want dock renderer in your window
            map.SetRenderingRect(new Rectangle(150, 0, 950, 600));
            map.Draw(Process.GetCurrentProcess().MainWindowHandle);

            timer.Elapsed += new ElapsedEventHandler(_timer_Elapsed);
            System.Threading.Thread.Sleep(3000);
            timer.Start();

            Console.ReadKey();
            
            //map.Render(Process.GetCurrentProcess().MainWindowHandle);
        }

        static void _timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            int r = rnd.Next();
            int x = 30, y = 30, z = 30;

        #if _3D
            byte[][][] mapMatrixl = new byte[x][][];
            for (int i = 0; i < x; i++)
            {
                mapMatrixl[i] = new byte[y][];
                for (int j = 0; j < y; j++)
                {
                    mapMatrixl[i][j] = new byte[z];
                    for (int k = 0; k < z; k++)
                    {
                        mapMatrixl[i][j][k] = (byte)((i + r) % 5);
                    }
                }
            }
        #else
            byte[][] mapMatrixl = new byte[x][];
            for (int i = 0; i < x; i++)
            {
                mapMatrixl[i] = new byte[y];
                for (int j = 0; j < y; j++)
                {
                    mapMatrixl[i][j] = (byte)((i + r) % 5);
                }
            }
        #endif
            map.UpdateData(mapMatrixl);
            Console.WriteLine("Update!!");
        }
    }
}
